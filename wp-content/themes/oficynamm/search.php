<?php get_header(); ?>
<?php if ( function_exists('yoast_breadcrumb') && ! is_front_page()) : ?>
   <div class="breadcrumbs">
       <div class="container">
            <?php yoast_breadcrumb('<p id="breadcrumbs">','</p>');?>
       </div>
   </div>
<?php endif ?>
<?php 
    global $wp_query;
?>
<div class="container page-news">
    <div class="row">
        <div class="col-lg-12 content no-padding">
            <div class="col-12">
                <h1 class="square-header">
                    Wyniki wyszukiwania
                </h1>
            </div>
            <div class="news">
                <div class="row">
                    <?php 
                        while(have_posts()): the_post(); 
                    ?>
                    <div class="col-md-6 single-news">
                        <a class="inner" href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('small-news-photo') ;?>
                            <div class="text">
                                <span class="date">
                                    <?php 
                                    if(get_post_type() == 'product') {
                                        echo 'Produkt';
                                    } else {
                                        echo 'Aktualność';
                                    }
                                    ?>
                                    <?php //the_time( get_option( 'date_format' ) ); ?>
                                </span>
                                <p class="title"><?php the_title(); ?></p>
                                <i class="fas fa-long-arrow-alt-right arrow"></i>
                            </div>
                        </a>
                    </div>
                    <?php
                        endwhile;
                    ?>
                </div>
            </div>
            <?php
                fellowtuts_wpbs_pagination();
            ?>
        </div>
        <?php //get_sidebar(); ?>
    </div>
</div>

<?php get_template_part('template-parts/online_platform');?>

<?php get_footer(); ?>