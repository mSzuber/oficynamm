<?php /* Template Name: Podstrona kategorii */ ?>
<?php get_header(); ?>
<div class="popular-categories category-product-popular-categories">
    <div class="container wide">
        <div class="col-12 small-big-header-wrapper black">
            <span class="header small">
                sprawdź
            </span>
            <span class="header big">
                najpopularniejsze kategorie
            </span>
        </div>
        <div class="row">
            <div class="col-lg-6 big">
                <a class="box content-hover" href="#" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/popular-categories-icons/school-bag.png')">
                    <h4 class="header">
                        oferta
                        <span class="main-title">
                            szkoły
                        </span>
                        <hr class="line">
                    </h4>
                </a>
            </div>
            <div class="col-lg-6 four-boxes no-padding">
                <div class="row">
                    <div class="box col-6">
                        <a class="inner content-hover" href="#" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/popular-categories-icons/crayons.png')">
                            <h4 class="header">
                                oferta
                                <span class="main-title">
                                    PRZEDSZKOLA
                                </span>
                                <hr class="line">
                            </h4>
                        </a>
                    </div>
                    <div class="box col-6">
                        <a class="inner content-hover" href="#" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/popular-categories-icons/gover.png')">
                            <h4 class="header">
                                oferta
                                <span class="main-title">
                                    URZĘDY
                                </span>
                                <hr class="line">
                            </h4>
                        </a>
                    </div>
                    <div class="box col-6">
                        <a class="inner content-hover" href="#" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/popular-categories-icons/house.png')">
                            <h4 class="header">
                                oferta
                                <span class="main-title">
                                    DOMY POMOCY SPOŁECZNEJ
                                </span>
                                <hr class="line">
                            </h4>
                        </a>
                    </div>
                    <div class="box col-6">
                        <a class="inner content-hover" href="#" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/popular-categories-icons/supp.png')">
                            <h4 class="header">
                                oferta
                                <span class="main-title">
                                    PORADNIE PSYCH. PEDAGOGICZNE
                                </span>
                                <hr class="line">
                            </h4>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="category-product container wide">
    <div class="row">
        <?php get_sidebar(); ?>
        <div class="col-lg-10 content">
            <div class="popular-products">
                <div class="col-12">
                    <h1 class="square-header">
                        Nazwa kategorii
                    </h1>
                </div>
                <div class="row products-section">
                    <div class="col-md-5 wide-section no-padding">
                        <div class="col-12 wide-product shop-product">
                            <div class="inner">
                                <div class="product-label-wrapper">
                                    <span class="product-label red">
                                        polecamy
                                    </span>
                                    <span class="product-label orange">
                                        polecamy
                                    </span>
                                    <span class="product-label pink">
                                        super cena
                                    </span>
                                </div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/big-photo-01.png')" alt>
                                <p class="title">
                                    Niezbędnik dyrektora szkoły. Zarządzanie oświatą
                                </p>
                                <div class="price-wrapper promotion">
                                    <span class="price">
                                        2599 zł
                                    </span>
                                    <span class="promotion-price">
                                        2499 zł
                                    </span>
                                </div>
                                <a class="shop-button orange" href="#">
                                    <i class="fas fa-shopping-basket"></i>
                                    do koszyka
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 narrow-section no-padding">
                        <div class="row">
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-01.png')" alt>
                                    </div>
                                    <p class="title">
                                        rodo w szkole. krok po kroku
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-02.png')" alt>
                                    </div>
                                    <p class="title">
                                        Oświatowy serwis kadrowy
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-03.png')" alt>
                                    </div>
                                    <p class="title">
                                        rodo w szkole. krok po kroku
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-01.png')" alt>
                                    </div>
                                    <p class="title">
                                        rodo w szkole. krok po kroku
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-02.png')" alt>
                                    </div>
                                    <p class="title">
                                        Oświatowy serwis kadrowy
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-03.png')" alt>
                                    </div>
                                    <p class="title">
                                        rodo w szkole. krok po kroku
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row products-section">
                    <div class="col-md-7 narrow-section no-padding">
                        <div class="row">
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-01.png')" alt>
                                    </div>
                                    <p class="title">
                                        rodo w szkole. krok po kroku
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-02.png')" alt>
                                    </div>
                                    <p class="title">
                                        Oświatowy serwis kadrowy
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-03.png')" alt>
                                    </div>
                                    <p class="title">
                                        rodo w szkole. krok po kroku
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-01.png')" alt>
                                    </div>
                                    <p class="title">
                                        rodo w szkole. krok po kroku
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-02.png')" alt>
                                    </div>
                                    <p class="title">
                                        Oświatowy serwis kadrowy
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 narrow-product shop-product">
                                <div class="inner">
                                    <div class="product-label-wrapper">
                                        <span class="product-label red">
                                            polecamy
                                        </span>
                                    </div>
                                    <div class="img-wrapper">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-03.png')" alt>
                                    </div>
                                    <p class="title">
                                        rodo w szkole. krok po kroku
                                    </p>
                                    <span class="price">
                                        195,57 zł
                                    </span>
                                    <a class="shop-button orange" href="#">
                                        <i class="fas fa-shopping-basket"></i>
                                        do koszyka
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 wide-section no-padding">
                        <div class="col-12 wide-product shop-product">
                            <div class="inner">
                                <div class="product-label-wrapper">
                                    <span class="product-label red">
                                        polecamy
                                    </span>
                                    <span class="product-label orange">
                                        polecamy
                                    </span>
                                    <span class="product-label pink">
                                        super cena
                                    </span>
                                </div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/big-photo-01.png')" alt>
                                <p class="title">
                                    Niezbędnik dyrektora szkoły. Zarządzanie oświatą
                                </p>
                                <div class="price-wrapper promotion">
                                    <span class="price">
                                        2599 zł
                                    </span>
                                    <span class="promotion-price">
                                        2499 zł
                                    </span>
                                </div>
                                <a class="shop-button orange" href="#">
                                    <i class="fas fa-shopping-basket"></i>
                                    do koszyka
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item prev">
                        <a class="page-link" href="#" aria-label="Previous">
                            <i class="fas fa-chevron-left"></i>
                        </a>
                    </li>
                    <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">3</a>
                    </li>
                    <li class="page-item next">
                        <a class="page-link" href="#" aria-label="Next">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="commercial-place">
                <div class="row">
                    <div class="col-12">
                        <img src="http://via.placeholder.com/1140x180" alt>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="online-platform">
    <div class="container wide">
        <div class="row">
            <div class="col-md-6 img-wrapper">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/online-platform.png" alt="">
            </div>
            <div class="col-md-6 content">
                <p class="text">
                    Czerp korzyści z naszej bazy wiedzy i dołącz do zadowolonych uzytkowników
                    <span>platformy online!</span>
                </p>
                <a class="button-arrow orange" href="#">
                    zobacz więcej
                    <i class="fas fa-long-arrow-alt-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="news home">
    <div class="container wide">
        <div class="row">
            <div class="col-lg-4 col-md-6 single-news">
                <a class="inner" href="#">
                    <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/news-image.jpeg" alt>
                    <div class="text">
                        <span class="date">02.03.2018</span>
                        <p class="title">Ćwiczenia dla uczniów niepełnosprawnych</p>
                        <i class="fas fa-long-arrow-alt-right arrow"></i>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 single-news">
                <a class="inner" href="#">
                    <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/news-image.jpeg" alt>
                    <div class="text">
                        <span class="date">02.03.2018</span>
                        <p class="title">Ćwiczenia dla uczniów niepełnosprawnych</p>
                        <i class="fas fa-long-arrow-alt-right arrow"></i>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 single-news">
                <a class="inner" href="#">
                    <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/news-image.jpeg" alt>
                    <div class="text">
                        <span class="date">02.03.2018</span>
                        <p class="title">Ćwiczenia dla uczniów niepełnosprawnych</p>
                        <i class="fas fa-long-arrow-alt-right arrow"></i>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 single-news">
                <a class="inner" href="#">
                    <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/news-image.jpeg" alt>
                    <div class="text">
                        <span class="date">02.03.2018</span>
                        <p class="title">Ćwiczenia dla uczniów niepełnosprawnych</p>
                        <i class="fas fa-long-arrow-alt-right arrow"></i>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>