<?php /* Template Name: Import Smartblog */
require_once( ABSPATH . '/wp-admin/includes/taxonomy.php');
require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');

global $wpdb; 
$presta = new wpdb('root','plokij12','presta','mysql');

// POST CATEGORIES 

$sql = "SELECT ps_smart_blog_category_lang.meta_title, ps_smart_blog_category_lang.meta_description, ps_smart_blog_category_lang.description,ps_smart_blog_category_lang.link_rewrite, ps_smart_blog_category.id_smart_blog_category
FROM ps_smart_blog_category
INNER JOIN ps_smart_blog_category_lang ON ps_smart_blog_category_lang.id_smart_blog_category = ps_smart_blog_category.id_smart_blog_category
WHERE ps_smart_blog_category_lang.id_lang = 1 AND ps_smart_blog_category_lang.link_rewrite != 'uncategories';";
$rows = $presta->get_results($sql);

$categories_ids = [];
foreach ($rows as $obj) :
	$cat_args = array(
	  'cat_name' => $obj->meta_title,
	  'category_description' => $obj->description,
	  'category_nicename' => $obj->link_rewrite,
	  'taxonomy' => 'category' 
	);
	$wp_category_id = wp_insert_category($cat_args);
	$categories_ids[$obj->id_smart_blog_category] = $wp_category_id;

endforeach;

//POST

$sql = "
SELECT 
ps_smart_blog_post.id_smart_blog_post,
ps_smart_blog_post.id_category, 
ps_smart_blog_post.modified, 
ps_smart_blog_post_lang.meta_title,
ps_smart_blog_post_lang.short_description,
ps_smart_blog_post_lang.content,
ps_smart_blog_post_lang.link_rewrite
FROM ps_smart_blog_post
INNER JOIN ps_smart_blog_post_lang ON ps_smart_blog_post_lang.id_smart_blog_post = ps_smart_blog_post.id_smart_blog_post
WHERE ps_smart_blog_post_lang.id_lang = 1
ORDER BY ps_smart_blog_post.id_smart_blog_post DESC
";

$rows = $presta->get_results($sql);

foreach($rows as $obj) {
	
	$post_args = array(
		'post_date'		=> $obj->modified,
		'post_content'	=> $obj->content,
		'post_title'	=> $obj->meta_title,
		'post_excerpt'	=> $obj->short_description,
		'post_status'	=> 'publish',
		'post_name'		=> $obj->link_rewrite,
		'post_category'	=> array($categories_ids[$obj->id_category])
	);

	$image_url = 'https://oficynamm.pl/upload/thumbs/960x540/smartblog/images/'.$obj->id_smart_blog_post.'.jpg';

	$post_id = wp_insert_post( $post_args );

	$attachment_id = media_sideload_image( $image_url, $post_id, null, 'id' );

	set_post_thumbnail( $post_id, $attachment_id );
}




