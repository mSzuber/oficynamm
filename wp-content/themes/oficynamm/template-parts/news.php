<?php
    $args = array(
        'post_type'         =>  'post',
        'posts_per_page'    =>  '6',
        'meta_query'        => array(
            array(
                'key'       => 'add_to_homepage',
                'value'     => true,
                'compare'   => '='
            )
        )
    );
    $blog_posts = new WP_Query($args);
?>
<div class="news home<?php echo is_front_page() ? ' home-background' : ''; ?>">
    <div class="container wide">
<!--         <span class="background-text">
            aktualności
        </span> -->
        <div class="row">
            <?php
                while($blog_posts->have_posts()): $blog_posts->the_post();
            ?>
            <div class="col-lg-4 col-md-6 single-news">
                <a class="inner" href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail('small-news-photo') ;?>
                    <div class="text">
                        <span class="date">
                            <?php the_time( get_option( 'date_format' ) ); ?>
                        </span>
                        <p class="title"><?php the_title(); ?></p>
                        <i class="fas fa-long-arrow-alt-right arrow"></i>
                    </div>
                </a>
            </div>
            <?php endwhile;wp_reset_postdata(); ?>
        </div>
        <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) );?> " class="button-arrow orange">Zobacz więcej <i class="fas fa-long-arrow-alt-right"></i></a>
    </div>
</div>