<?php
$big_tile_id        = get_field('duzy_kafelek','option');
$small_tiles_ids    = get_field('4_male_kafelki','option');

if( !empty($big_tile_id) ){

    $big_tile      = get_term($big_tile_id,'product_cat');

} else {

    $big_tile      = get_terms('product_cat',array(
        'orderby' => 'rand',
        'number'  => '1'
    ));

}
if( sizeof($small_tiles_ids) < 4) {

        $small_tiles = get_terms('product_cat', array(
            'orderby'   => 'rand',
            'number'    => 4 - sizeof($small_tiles_ids)
        ));
} else {
        $small_tiles      = get_terms('product_cat',array(
            'include'   => $small_tiles_ids,
        ));
}

?>
<div class="popular-categories category-product-popular-categories">
    <div class="container wide">
        <div class="col-12 small-big-header-wrapper black">
            <span class="header small">
                sprawdź
            </span>
            <span class="header big">
                najpopularniejsze kategorie
            </span>
        </div>
        <div class="row">
            <?php
            $category_thumb_id = get_woocommerce_term_meta( $big_tile->term_id, 'thumbnail_id', true );
            $category_thumb = wp_get_attachment_url( $category_thumb_id );
            ?>
            <div class="col-lg-6 big">
                <a class="box content-hover" href="<?php echo get_term_link($big_tile);?>" style="background-image: url('<?php echo $category_thumb;?>')">
                    <h4 class="header">
                        oferta
                        <span class="main-title">
                            <?php echo $big_tile->name;?>
                        </span>
                        <hr class="line">
                    </h4>
                </a>
            </div>
            <div class="col-lg-6 four-boxes no-padding">
                <div class="row">
                    <?php foreach($small_tiles as $small_tile):?>
                        <?php
                        $category_thumb_id = get_woocommerce_term_meta( $small_tile->term_id, 'thumbnail_id', true );
                        $category_thumb = wp_get_attachment_url( $category_thumb_id );
                        ?>
                        <div class="box col-6">
                            <a class="inner content-hover" href="<?php echo get_term_link($small_tile);?>" style="background-image: url('<?php echo $category_thumb;?>')">
                                <h4 class="header">
                                    oferta
                                    <span class="main-title">
                                        <?php echo $small_tile->name;?>
                                    </span>
                                    <hr class="line">
                                </h4>
                            </a>
                        </div>
                    <?php endforeach;?>

                </div>
            </div>
            <?php if(is_front_page()) :?>
            <div class="col-md-6 medium">
                    <a href="#" class="box content-hover" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/popular-categories-icons/software-house.png')">
                        <h4 class="header">
                            DEMO
                            <span class="main-title">
                                PRZETESTUJ NASZE OPROGRAMOWANIE
                            </span>
                            <hr class="line">
                        </h4>
                    </a>
            </div>
            <div class="col-md-6 medium">
                <a href="#" class="box content-hover" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/popular-categories-icons/judge.png')">
                    <h4 class="header">
                        Porady
                        <span class="main-title">
                            bezpłatne, indywidualne konsultacje z prawnikiem
                        </span>
                        <hr class="line">
                    </h4>
                </a>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>