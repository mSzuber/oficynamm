<?php
  $posts_per_page = 8;
    $args = array(
        'posts_per_page'    => $posts_per_page,
        'post_type'         => 'product',
        'post__not_in'      => array(get_the_ID()),
        'orderby'           => 'rand',
        'meta_query'        => array(
            array(
                'key'       => 'is_featured',
                'value'     => true,
                'compare'   => '='
            )
        ),
    );
    $products_loop = new WP_Query($args);

?>
<?php if( $products_loop->have_posts() ):?>
    <div class="popular-products chosen-product-popular-products">
        <div class="container wide">
            <div class="col-12 small-big-header-wrapper black">
                <span class="header small">
                    sprawdź
                </span>
                <span class="header big">
                    polecane produkty
                </span>
            </div>
            <div class="row products-section">
                <div class="col-md-12 narrow-section no-padding">
                    <div class="row">
                        <?php
                        while($products_loop->have_posts()) : $products_loop->the_post(); ?>
                            <?php 
                                        global $woocommerce;
                                        $_product = wc_get_product(get_the_ID());
                                        $price  = false;
                                        $sale   = false;
                                        if( $_product->is_type('variable') ) {
                                            $price = $_product->get_price();
                                        } else {
                                            $price = get_post_meta( get_the_ID(), '_regular_price', true);
                                            $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                                        }

                                        $url = get_permalink(get_the_ID());
                        ?>
                            <div class="col-md-3 narrow-product shop-product">
                                <div class="inner">
                                    <?php
                                        $labels = get_field('wybierz_label');
                                    ?>
                                        <?php if(!empty($labels)) : ?>
                                        <div class="product-label-wrapper">
                                            <?php foreach($labels as $label): ?>
                                            <span class="product-label" style="background-color:<?php echo $label['value']; ?>">
                                                <?php echo $label['label']; ?>
                                            </span>
                                            <?php endforeach; ?>
                                        </div>
                                        <?php endif; ?>
                                        <a href="<?php echo $url; ?>" class="img-wrapper">
                                            <?php the_post_thumbnail('small-product-photo') ?>
                                        </a>
                                        <a href="<?php echo $url; ?>" class="title" title="<?php echo the_title(); ?>">
                                        <?php
                                            $string = get_the_title(); 
                                            if(strlen($string) > 50) {
                                                $string = substr($string, 0, 50) . '...';
                                            }
                                            echo $string;
                                         ?>
                                        </a>
                                        <?php if($sale) : ?>
                                        <span class="price">
                                            <del class="old-price">
                                                <?php echo wc_price($price); ?>
                                            </del>
                                                <?php echo wc_price($sale); ?>
                                        </span>
                                        <?php elseif($price) : ?>
                                        <span class="price">
                                            <?php  
                                            if( $_product->is_type('variable') ) {
                                                echo 'od ';
                                            }
                                            ?>
                                                <?php echo wc_price($price); ?>
                                        </span>
                                        <?php endif; ?>
                                        <?php if( $_product->is_type('variable') ): ?>
                                            <a class="shop-button orange" href="<?php the_permalink();?>">
                                                <i class="fas fa-eye"></i>
                                                zobacz
                                            </a>                                            
                                        <?php else:?>
                                            <a class="shop-button orange" href="<?php echo $_product->add_to_cart_url();?>">
                                                <i class="fas fa-shopping-basket"></i>
                                                do koszyka
                                            </a>
                                        <?php endif;?>
                                </div>
                            </div>
                            <?php  endwhile;wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif;?>