<div class="online-platform" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/img/mobile-platforms-bg.png');">
    <div class="container wide">
        <div class="row">
            <div class="col-md-6 img-wrapper">
                <?php echo wp_get_attachment_image( get_field('po_zdjecie','option') , 'full' ); ?>
            </div>
            <div class="col-md-6 content">
                <p class="text">
                    <?php the_field('po_mniejszy_tekst','option');?>
                    <span><?php the_field('po_duzy_tekst','option');?></span>
                </p>
                <a class="button-arrow orange" href="<?php the_field('po_link','option');?>">
                    zobacz więcej
                    <i class="fas fa-long-arrow-alt-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>