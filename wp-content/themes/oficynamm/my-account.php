<?php /* Template Name: Moje konto */ ?>
<?php get_header(); ?>
<div class="my-account">
    <div class="container">
        <div class="col-12 header-wrapper">
            <h1 class="header">moje konto</h1>
            <hr class="line">
        </div>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" id="files-to-download-tab" data-toggle="tab" href="#files-to-download" role="tab" aria-controls="files-to-download"
                    aria-selected="true">Pliki do pobrania</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" id="orders-tab" data-toggle="tab" href="#orders" role="tab" aria-controls="orders" aria-selected="false">Zamówienia</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="false">Moje dane</a>
            </li>
        </ul>
        <div class="col-12">
            <h1 class="square-header">
                Zamówienia
            </h1>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade" id="files-to-download" role="tabpanel" aria-labelledby="files-to-download">1</div>
            <div class="tab-pane fade show active" id="orders" role="tabpanel" aria-labelledby="orders-tab">
                <div class="row ordered-product paid">
                    <div class="col-3 image">
                        <img src="http://via.placeholder.com/200x225" alt>
                    </div>
                    <div class="col-3-half desc">
                        Pakiet dla przedszkoli: Niezbędnik dyrektora placówki niepulicznej
                    </div>
                    <div class="price col-2-half">
                        <span class="small-text">Wartość:</span>
                        <span class="value">11202,69 zł</span>
                    </div>
                    <div class="status col-2-half">
                        <span class="small-text">Status:</span>
                        <span class="pay">
                            Zapłacono
                        </span>
                        <span class="not-pay">
                            Nieopłacone
                        </span>
                    </div>
                    <div class="col-2 buttons">
                        <a href="#" class="small-button orange">
                            ponów
                        </a>
                        <a href="#" class="small-button black">
                            zapłać
                        </a>
                    </div>
                    <div class="line-wrapper col-12">
                        <hr class="line">
                    </div>
                </div>
                <div class="row ordered-product unpaid">
                    <div class="col-3 image">
                        <img src="http://via.placeholder.com/200x225" alt>
                    </div>
                    <div class="col-3-half desc">
                        pakiet dla przedszkoli: Niezbędnik dyrektora placówki niepublicznej
                    </div>
                    <div class="col-2-half price">
                        <span class="small-text">Wartość:</span>
                        <span class="value">11202,69 zł</span>
                    </div>
                    <div class="col-2-half status">
                        <span class="small-text">Status:</span>
                        <span class="pay">
                            Zapłacono
                        </span>
                        <span class="not-pay">
                            Nieopłacone
                        </span>
                    </div>
                    <div class="col-2 buttons">
                        <a class="small-button orange" href="#">
                            ponów
                        </a>
                        <a class="small-button black" href="#">
                            zapłać
                        </a>
                    </div>
                    <div class="line-wrapper col-12">
                        <hr class="line">
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="account" role="tabpanel" aria-labelledby="account-tab">3</div>
        </div>
        <nav>
            <ul class="pagination">
                <li class="page-item prev">
                    <a class="page-link" href="#" aria-label="Previous">
                        <i class="fas fa-chevron-left"></i>
                    </a>
                </li>
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">3</a>
                </li>
                <li class="page-item next">
                    <a class="page-link" href="#" aria-label="Next">
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<?php get_footer(); ?>