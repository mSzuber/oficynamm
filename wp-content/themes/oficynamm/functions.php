<?php
require_once 'inc/boostrap-pagination.php';
require_once 'inc/wp-bootstrap-navwalker.php';
require_once 'inc/functions_woocommerce.php';
require_once 'inc/ads_widgets.php';

add_theme_support( 'post-thumbnails' );
add_image_size('small-product-photo', 160, 165, true);
add_image_size( 'main-news-photo', 350, 265, true );
add_image_size( 'small-news-photo', 95, 95, true);

function theme_name_scripts() {
    wp_enqueue_style( 'font-biryani', 'https://fonts.googleapis.com/css?family=Biryani:200,300,400,600,700,800,900&amp;subset=latin-ext');  
    wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/fonts/fontawesome-free-5.0.13/web-fonts-with-css/css/fontawesome-all.min.css');  
    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri().'/css/bootstrap.min.css');
    wp_enqueue_style( 'nouislider-css', get_template_directory_uri().'/css/nouislider.min.css');
    wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri().'/css/owl.carousel.min.css');
    wp_enqueue_style( 'style-css', get_template_directory_uri().'/css/style.css');
    wp_enqueue_script( 'popper-js', get_template_directory_uri() . '/js/popper.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'nouislider.js', get_template_directory_uri() . '/js/nouislider.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );



// ACF to json

add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point( $path ) {
   
   // update path
   $path = get_stylesheet_directory() . '/acf-json';
   
   
   // return
   return $path;
   
}


add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {
   
   // remove original path (optional)
   unset($paths[0]);
   
   
   // append path
   $paths[] = get_stylesheet_directory() . '/acf-json';
   
   
   // return
   return $paths;
   
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page('Ustawienia szablonu');
	
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// Menu

add_action( 'after_setup_theme', 'register_custom_menus' );
function register_custom_menus() {
  register_nav_menus( 
    array(
      'footer-shop'         => __( 'Stopka - Sklep', 'oficyna' ),
      'footer-categories'   => __( 'Stopka - Kategorie', 'oficyna' ),
      //'footer-bottom'      => __('Stopka - menu poziome', 'oficyna'),
      'header'              => __( 'Główne menu', 'oficyna' )
    )
  );
}

//Ads


/**
 * Register our sidebars and widgetized areas.
 *
 */
function sw_widgets_init() {

  register_sidebar( array(
    'name'          => 'Sklep sidebar',
    'id'            => 'shop_sidebar',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '',
    'after_title'   => '',
  ) );

  register_sidebar( array(
    'name'          => 'Aktualności sidebar',
    'id'            => 'news_sidebar',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '',
    'after_title'   => '',
  ) );

  register_sidebar( array(
    'name'          => 'Sklep na dole',
    'id'            => 'shop_bottom',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '',
    'after_title'   => '',
  ) );  

  register_sidebar( array(
    'name'          => 'Produkt na dole',
    'id'            => 'product_bottom',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '',
    'after_title'   => '',
  ) );  

}
add_action( 'widgets_init', 'sw_widgets_init' );


add_action('woocommerce_after_shop_loop_item', 'get_star_rating' );


function my_pre_get_posts($query) {

    if( is_admin() ) 
        return;

    if( is_search() && $query->is_main_query() ) {
        $query->set('post_type', array( 'post' , 'product' ));
    } 

}

add_action( 'pre_get_posts', 'my_pre_get_posts' );


/**
 * @snippet       Add privacy policy tick box at checkout
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=19854
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.3.4
 */
 
add_action( 'woocommerce_review_order_before_submit', 'bbloomer_add_checkout_privacy_policy', 9 );
   
function bbloomer_add_checkout_privacy_policy() {
  
woocommerce_form_field( 'privacy_policy', array(
    'type'          => 'checkbox',
    'class'         => array('form-row privacy'),
    'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
    'required'      => true,
    'label'         => 'I\'ve read and accept the <a href="/privacy-policy">Privacy Policy</a>',
)); 

woocommerce_form_field( 'shop_rules', array(
    'type'          => 'checkbox',
    'class'         => array('form-row shop-rules'),
    'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
    'required'      => true,
    'label'         => 'I\'ve read and accept the <a href="/privacy-policy">shop rules</a>',
));   
}
  
// Show notice if customer does not tick
   
add_action( 'woocommerce_checkout_process', 'bbloomer_not_approved_privacy' );
  
function bbloomer_not_approved_privacy() {
    if ( ! (int) isset( $_POST['privacy_policy'] ) ) {
        wc_add_notice( __( 'Please acknowledge the Privacy Policy' ), 'error' );
    }
}

add_action( 'woocommerce_checkout_process', 'bbloomer_not_approved_shop_rules' );
  
function bbloomer_not_approved_shop_rules() {
    if ( ! (int) isset( $_POST['shop_rules'] ) ) {
        wc_add_notice( __( 'Please acknowledge the Privacy Policy' ), 'error' );
    }
}



