<?php get_header(); ?>
<?php if ( function_exists('yoast_breadcrumb') && ! is_front_page()) : ?>
   <div class="breadcrumbs">
       <div class="container">
            <?php yoast_breadcrumb('<p id="breadcrumbs">','</p>');?>
       </div>
   </div>
<?php endif; ?>
</div>
<div class="woocommerce-steps">
    <div class="container wide">
        <?php the_content(); ?>
    </div>
</div>
<?php get_footer(); ?>