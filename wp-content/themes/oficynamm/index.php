<?php get_header(); ?>
<?php if ( function_exists('yoast_breadcrumb') && ! is_front_page()) : ?>
   <div class="breadcrumbs">
       <div class="container">
            <?php yoast_breadcrumb('<p id="breadcrumbs">','</p>');?>
       </div>
   </div>
<?php endif ?>
<?php 
    global $wp_query;
?>
<div class="container page-news">
    <div class="row">
        <div class="col-lg-9 content no-padding">
            <div class="col-12">
                <h1 class="square-header">
                    <?php if( is_category() ) {
                        echo single_cat_title();
                    } else {
                        echo 'Aktualności';
                    }
                    ?>
                </h1>
            </div>
            <?php
                while(have_posts()): the_post(); 
            ?>
            <div class="main-news">
                <?php the_post_thumbnail('main-news-photo') ;?>
                <div class="text">
                    <span class="date">
                        <?php the_time( get_option( 'date_format' ) ); ?>
                    </span>
                    <a href="<?php the_permalink(); ?>" class="inner">
                        <h3 class="title">
                            <?php the_title(); ?>
                        </h3>
                        <p><?php echo strip_tags( get_the_excerpt() ); ?></p>
                        <i class="fas fa-long-arrow-alt-right arrow"></i>
                    </a>
                </div>
            </div>
            <?php 
                break;
                endwhile; 
            ?>
            <div class="news">
                <div class="row">
                    <?php 
                        while(have_posts()): the_post(); 
                    ?>
                    <div class="col-md-6 single-news">
                        <a class="inner" href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('small-news-photo') ;?>
                            <div class="text">
                                <span class="date">
                                    <?php the_time( get_option( 'date_format' ) ); ?>
                                </span>
                                <p class="title"><?php the_title(); ?></p>
                                <i class="fas fa-long-arrow-alt-right arrow"></i>
                            </div>
                        </a>
                    </div>
                    <?php
                        endwhile;
                    ?>
                </div>
            </div>
            <?php
                fellowtuts_wpbs_pagination();
            ?>
        </div>
        <?php get_sidebar(); ?>
    </div>
</div>

<?php get_template_part('template-parts/online_platform');?>

<?php get_footer(); ?>