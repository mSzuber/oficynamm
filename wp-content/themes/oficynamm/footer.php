<footer class="footer">
    <div class="newsletter red-bg">
        <div class="container wide">
            <div class="row inner">
                <span class="header upper-case">newsletter</span>
                <span class="text">
                    Otrzymuj najnowsze informacje z naszego wydawnictwa
                </span>
                <?php ?>
                <div class="email-wrapper" id="fm_form_1">
                    <div class="form_container">
                        <form method="post" class="form_subscribe freshmail_form_1">
                            <input class="email-input field" placeholder="Podaj swój adres email" name="form[email]" type="text">
                            <input type="hidden" value="1" name="fm_form_id"/>
                            <input type="hidden" value="fm_form" name="action"/>
                            <input type="hidden" value="/" name="fm_form_referer"/>
                            <button class="small-button orange form_subscribe_button" name="form_subscribe_button" type="submit">Zapisz</button>
                            <input class="checkbox" type="checkbox" name="agree" id="agree-checkbox">
                            <label for="agree-checkbox">
                                Wyrażam zgodę
                                <br> na
                                <a class="link" href="#">
                                    przetwarzanie danych
                                </a>
                            </label>
                        </form>
                    </div>
                </div>
                <?php  ?>
                <?php //echo do_shortcode('[FM_form id="1"]');?>
            </div>
        </div>
    </div>
    <div class="light-black-bg">
        <div class="top-footer container wide">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <ul class="contact-info">
                        <li class="address">
                            <img src="<?php echo get_stylesheet_directory_uri();?>/img/footer-icons/house.png" alt="">
                            <p class="main-text">Adres firmy</p>
                            <p class="sub-text">
                                <?php the_field('adres_1_linia','option');?>
                                <br>
                                <?php the_field('adres_2_linia','option');?>
                            </p>
                        </li>
                        <li class="telephone">
                            <img src="<?php echo get_stylesheet_directory_uri();?>/img/footer-icons/phone-call.png" alt="">
                            <p class="sub-text">
                                Zadzwoń
                            </p>
                            <a class="main-text" href="tel:<?php the_field('numer_telefonu','option');?>">
                                <?php the_field('numer_telefonu','option');?>
                                </>
                            </a>
                        </li>
                        <li class="message">
                            <img src="<?php echo get_stylesheet_directory_uri();?>/img/footer-icons/envelope.png" alt="">
                            <p class="sub-text">Napisz</p>
                            <a class="main-text" href="mailto:biuro@oficynamm.pl">
                                <?php the_field('adres_email','option');?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <h5 class="header upper-case top-footer-header">
                        informacje
                    </h5>
                    <?php
                    wp_nav_menu(array(
                        'theme_location'    => 'footer-categories',
                        'depth'             => 1,
                        'menu_class'        => 'footer-default-list',
                        'container'         => '',
                        'container_class'   => '',
                    ))
                    ?>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <h5 class="header upper-case top-footer-header">
                        sklep
                    </h5>
                    <?php
                    wp_nav_menu(array(
                        'theme_location'    => 'footer-shop',
                        'depth'             => 1,
                        'menu_class'        => 'footer-default-list',
                        'container'         => '',
                        'container_class'   => '',
                    ))
                    ?>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <h5 class="header top-footer-header">
                        Znajdź nas na Facebooku
                    </h5>
                    <ul class="footer-fb-list">
                        <?php while( have_rows('footer_fb_link','option') ): the_row();?>
                            <li>
                                <a href="<?php the_sub_field('link');?>" target="_blank">
                                    <?php the_sub_field('tekst');?>
                                </a>
                            </li>
                        <?php endwhile;?>
                    </ul>
                </div>
            </div>
        </div>
        <hr class="line">
        <div class="bottom-footer container wide">
            <div class="inner">
                <div class="company-info">
                    <a class="footer-logo" href="<?php echo home_url();?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer-logo.png" alt>
                    </a>
                    <span class="text">
                        Copyright 2018
                        <br> Oficyna MM Wydawnictwo Prawniczne
                    </span>
                </div>
                <div class="designer">
                    <p>
                        Realizacja: 
                    </p>
                    <a href="https://www.e-conex.com/"></a>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-e-conex.png" alt>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>