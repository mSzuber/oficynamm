<?php get_header(); ?>
<?php if ( function_exists('yoast_breadcrumb') && ! is_front_page()) : ?>
   <div class="breadcrumbs shop-product">
       <div class="container">
            <?php yoast_breadcrumb('<p id="breadcrumbs">','</p>');?>
       </div>
   </div>
<?php endif ?>
<?php
    $meta_query = array();

    if(isset($_GET['price-min']) && isset($_GET['price-max'])) {
        $meta_query[] = array(
                'key'       =>  '_price',
                'value'     =>  (int)$_GET['price-min'],
                'compare'   =>  '>=',
                'type'      => 'numeric'
        );

        $meta_query[] = array(
                'key'       =>  '_price',
                'value'     =>  (int)$_GET['price-max'],
                'compare'   =>  '<=',
                'type'      => 'numeric'
        );
        
    }

    if(isset( $_GET['filters']) ) {

        if( $_GET['filters'] == 'popular' ) {
            $meta_query[] = array(
                'key'       =>  'is_popular',
                'value'     =>  '1',
                'compare'   =>  '=',
            );              
        }

        if( $_GET['filters'] == 'featured' ) {
            $meta_query[] = array(
                'key'       =>  'is_featured',
                'value'     =>  '1',
                'compare'   =>  '=',
            );                  
        }     
    }

    global $wp_query;
    $args = array_merge( $wp_query->query_vars, array( 'meta_query' => $meta_query ) );
    query_posts( $args );
?>


<div class="category-product container wide">
    <div class="row">
        <?php get_sidebar( 'shop' ); ?>
        <div class="col-lg-10 content">
            <div class="popular-products">
                <div class="col-12">
                    <h1 class="square-header">
                        <?php if(is_product_category()) {
                            single_term_title();
                        } else {
                            echo 'Wszystkie produkty';
                        } ?>
                    </h1>
                    <a href="#" class="filters">
                        filtry
                    </a>
                </div>
                    <div class="row products-section section-left">
                        <div class="col-md-7 narrow-section no-padding">
                            <div class="row">
                                <?php 
                                    while( have_posts() ): the_post(); 
                                        global $woocommerce;
                                        $_product = wc_get_product(get_the_ID());
                                        $price  = false;
                                        $sale   = false;
                                        if( $_product->is_type('variable') ) {
                                            $price = $_product->get_price();
                                        } else {
                                            $price = get_post_meta( get_the_ID(), '_regular_price', true);
                                            $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                                        }

                                        $url = get_permalink(get_the_ID());
                                        
                                ?>
                                <div class="col-md-3 narrow-product shop-product">
                                    <div class="inner">
                                        <?php
                                            $labels = get_field('wybierz_label');
                                        ?>
                                        <?php if(!empty($labels)) : ?>
                                        <div class="product-label-wrapper">
                                            <?php foreach($labels as $label): ?>
                                            <span class="product-label" style="background-color:<?php echo $label['value']; ?>">
                                                <?php echo $label['label']; ?>
                                            </span>
                                            <?php endforeach; ?>
                                        </div>
                                        <?php endif; ?>
                                        <a href="<?php echo $url; ?>" class="img-wrapper">
                                            <?php the_post_thumbnail('small-product-photo') ?>
                                        </a>
                                        <a href="<?php echo $url; ?>" class="title" title="<?php echo the_title(); ?>">
                                        <?php
                                            $string = get_the_title();
                                            if(mb_strlen($string) > 35) {
                                                $string = mb_substr($string, 0, 35) . '...';
                                            }
                                            echo $string;
                                         ?>
                                        </a>
                                        <?php if($sale) : ?>
                                        <span class="price">
                                            <del class="old-price">
                                                <?php echo wc_price($price); ?>
                                            </del>
                                                <?php echo wc_price($sale); ?>
                                        </span>
                                        <?php elseif($price) : ?>
                                        <span class="price">
                                            <?php  
                                            if( $_product->is_type('variable') ) {
                                                echo 'od ';
                                            }
                                            ?>
                                                <?php echo wc_price($price); ?>
                                        </span>
                                        <?php endif; ?>
                
                                        <?php if( $_product->is_type('variable') || !$price ): ?>
                                            <a class="shop-button orange" href="<?php the_permalink();?>">
                                                <i class="fas fa-eye"></i>
                                                zobacz
                                            </a>                                            
                                        <?php else:?>
                                            <a class="shop-button orange" href="<?php echo $_product->add_to_cart_url();?>">
                                                <i class="fas fa-shopping-basket"></i>
                                                do koszyka
                                            </a>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <?php endwhile;?>
                            </div>
                        </div>
                    </div>
            </div>
            <?php
                fellowtuts_wpbs_pagination();
            ?>
                <?php if ( is_active_sidebar( 'shop_bottom' ) ) : ?>
                <div class="commercial-place">
                    <div class="row">
                        <div class="col-12">
                        <?php dynamic_sidebar( 'shop_bottom' ); ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
        </div>
    </div>
</div>
<?php get_template_part('template-parts/popular_categories');?>

<?php get_template_part('template-parts/online_platform');?>

<?php get_template_part('template-parts/news');?>

<?php get_footer(); ?>