<?php get_header(); ?>
<?php if ( function_exists('yoast_breadcrumb') && ! is_front_page()) : ?>
   <div class="breadcrumbs shop-product">
       <div class="container">
            <?php yoast_breadcrumb('<p id="breadcrumbs">','</p>');?>
       </div>
   </div>
<?php endif ?>
<?php
    while(have_posts()) : the_post();
    global $product;
    $attachment_ids = $product->get_gallery_attachment_ids();
    array_unshift($attachment_ids, get_post_thumbnail_id());
    $duration = $product->get_attribute('okres');
    $duration = explode('|', $duration);
?>
<div class="chosen-product">
    <div class="container wide">
        <div class="row">
            <div class="col-lg-6 image">
                <div class="product-big-photo">
                <?php
                    // Add a filter to remove srcset attribute from generated <img> tag
                    add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );

                    // Display post thumbnail
                    the_post_thumbnail();

                    // Remove that filter again
                    remove_filter( 'wp_calculate_image_srcset_meta', '__return_null' );
                ?>
                </div>
                <div class="slider-wrapper">
                    <div class="owl-carousel owl-theme" id="pictorial-photos-slider">
                        <?php foreach($attachment_ids as $single_photo):?>
                        <div class="item">
                            <?php echo wp_get_attachment_image($single_photo, 'full'); ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 desc">
                <h1 class="header">
                     <?php the_title(); ?>
                </h1>
                <div class="button-wrapper">
                    <a class="big-button orange-bg show-variants" data-show-tab="nav-variants-tab" href="#">
                        <i class="fas fa-shopping-basket"></i>
                        pokaż warianty
                    </a>
                </div>
                <p>
                    <?php
                        the_excerpt();
                    ?>
                </p>
                <a class="link description-link" data-show-tab="nav-information-tab" href="#">
                    zobacz pełny opis
                </a>
            </div>
        </div>
    </div>
</div>

<?php

$labels = [];
$options = array(
    array(),
    array(),
    array()
);

while( have_rows('pakiety_wersja_2') ) { the_row();
    $labels[]     = get_sub_field('opis_wiersza');
    $options[0][] = get_sub_field('pakiet_1_-_wartosc');
    $options[1][] = get_sub_field('pakiet_2_-_wartosc');
    $options[2][] = get_sub_field('pakiet_3_-_wartosc');

}
?>
<div class="about-product">
    <div class="container wide">
        <div class="border-content">
            <div class="nav nav-tabs" id="about-product" role="tablist">
                <a class="nav-item nav-link active" id="nav-variants-tab" href="#nav-variants">Warianty</a>
                <a class="nav-item nav-link" id="nav-information-tab" href="#nav-information">Opis</a>
<!--                 <a class="nav-item nav-link" id="nav-office-tab" href="#nav-office">Informacje dodatkowe
                </a>
                <a class="nav-item nav-link" id="nav-opinion-tab" href="#nav-opinion">Archiwum Wydań</a> -->
            </div>
            <div class="tab-content" id="about-product-content">
                <div class="tab-pane active" id="nav-variants">
                    <div class="tab-inner">
                        <div class="row">
                            <div class="col-lg-3 left">
                                <div class="inner">
                                    <ul class="option-list">
                                    <?php foreach($labels as $label):?>
                                        <li>
                                            <?php if(strlen($label) > 35):?>
                                                <span class="expand-content show" href="#">
                                                    +
                                                </span>
                                            <?php endif;?>
                                            <span class="all-content show">
                                                <?php echo $label; ?>
                                            </span>
                                        </li>
                                    <?php endforeach;?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-9 no-padding wrapper">
                                <div class="product-duration active">
                                    <span class="text">Wybierz okres</span>
                                    <?php 
                                        foreach($duration as $index => $single_duration):
                                    ?>
                                    <label class="input-wrapper active" for="six-months<?php echo $index; ?>">
                                        <input id="six-months<?php echo $index; ?>" name="months" type="radio" value="<?php echo $single_duration; ?>" <?php echo ($index == 0) ? 'checked' : '' ; ?>>
                                            <?php echo $single_duration; ?>
                                        <span class="custom-input"></span>
                                    </label>
                                        <?php endforeach; ?>
                                </div>
                                <?php
                                    $variations = $product->get_available_variations();
                                    $variants_grouped = [];
                                    foreach($variations as $variant) {
                                        $variants_grouped[$variant['attributes']['attribute_okres']][] = $variant;
                                    }
                                ?>
                                <?php
                                    $k = 0;
                                    foreach($variants_grouped as $group):
                                ?>
                                <div class="row pricing<?php echo($k == 0) ? ' chosen' : ''; ?>" data-offer="six-months<?php echo $k; ?>">

                                <?php
                                    $i = 0;
                                    foreach($group as $variant):
                                    $duration = $variant['attributes']['attribute_okres'];
                                    $duration = explode(' ', $duration);
                                    $product_variation = new WC_Product_Variation($variant['variation_id']);
                                    $regular_price = $product_variation->regular_price;
                                    $sale_price = $product_variation->sale_price;
                                ?>
                                    <div class="box <?php echo (sizeof($group) == 2)? 'col-lg-6':'col-lg-4';?>">
                                        <div class="inner">
                                            <div class="offer-name">
                                                <span>
                                                    <?php
                                                        $packet_name = $variant['attributes']['attribute_pakiet'];
                                                        $packet_name = htmlentities($packet_name, null, 'utf-8');
                                                        $packet_name = str_replace("&nbsp;", "", $packet_name);
                                                        echo $packet_name.' '.$duration[0];
                                                    ?>
                                                </span>
                                            </div>
                                            <ul class="about-product-list">
                                            <?php
                                                $j = 0;
                                                foreach($options[$i] as $option):?>
                                            <li>
                                                <span class="mobile">
                                                    <?php echo $labels[$j]; ?>
                                                </span>

                                                <span class="content">
                                                <?php if(strtolower($option[$duration[0].'_msc']) == 'tak'): ?>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png" alt>
                                                <?php elseif(strtolower($option[$duration[0].'_msc']) == 'nie' || $option[$duration[0].'_msc'] == ''):?>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/gray-x.png" alt>
                                                <?php else: ?>
                                                    <?php echo $option[$duration[0].'_msc']; ?>
                                                <?endif; ?>
                                                </span>
                                            </li>
                                            <?php 
                                                $j++;endforeach;
                                            ?>
                                            </ul>
                                            <div class="summary-price">
                                                <span class="price">
                                                    <?php 
                                                        if($sale_price != $regular_price && $sale_price != 0):
                                                    ?>
                                                    <span class="sale">
                                                        <?php echo wc_price($sale_price); ?>
                                                    </span>
                                                    <del class="old-price">
                                                        <?php echo wc_price($regular_price); ?>
                                                    </del>
                                                    <?php else:?>
                                                        <?php echo wc_price($regular_price); ?>
                                                    <?php endif;   ?>
                                                </span>
                                                <?php
                                                    $attribute_okres = $product_variation->attributes['okres'];
                                                    $attribute_pakiet = $product_variation->attributes['pakiet'];
                                                    $variation_id = $product_variation->ID;
                                                    $product_id = get_the_ID();
                                                    $cart_url = get_permalink( woocommerce_get_page_id( 'cart' ) );
                                                    $add_to_cart_url = $cart_url.'/?add-to-cart='.$product_id.'&variation_id='.$variation_id.'&attribute_okres='.$attribute_okres.'&attribute_pakiet='.$attribute_pakiet.'&quantity=1';
                                                ?>
                                                <a class="button orange-bg" href="<?php echo $add_to_cart_url; ?>">
                                                    wybierz
                                                </a>
                                            </div>
                                            <div class="box-shadow">
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                        if($i == 2) {
                                            $i = 0;
                                        }else {
                                            $i++;
                                        }
                                        endforeach; 
                                    ?>
                                </div>
                                <?php 
                                    $k++;
                                    endforeach; 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane page-content" id="nav-information">
                    <p>
                        <?php the_content(); ?>
                    </p>
                </div>
                <div class="tab-pane" id="nav-office">
                    <?php the_field('redakcja'); ?>
                </div>
                <div class="tab-pane" id="nav-opinion">
                    <?php the_field('pole_tekstowe');?>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php get_template_part('template-parts/featured_products');?>


    <?php if ( is_active_sidebar( 'product_bottom' ) ) : ?>
        <div class="commercial-place chosen-product-commercial">
            <div class="container wide">
                <div class="row">
                        <?php dynamic_sidebar( 'product_bottom' ); ?>
                </div>
            </div>
        </div>
    <?php endif;?>
<?php endwhile; ?>
<?php get_footer(); ?>