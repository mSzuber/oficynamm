<?php
    
    $product_id = get_the_ID(); // the ID of the product to check
    $_product = wc_get_product( $product_id );
    if( $_product->is_type( 'variable' ) ) {
    get_template_part( 'woocommerce/single-product-variants' );
    } else {
    get_template_part( 'woocommerce/single-product-simple' );
    }