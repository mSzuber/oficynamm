<?php get_header(); ?>
<?php if ( function_exists('yoast_breadcrumb') && ! is_front_page()) : ?>
   <div class="breadcrumbs shop-product">
       <div class="container">
            <?php yoast_breadcrumb('<p id="breadcrumbs">','</p>');?>
       </div>
   </div>
<?php endif ?>
<?php
    while(have_posts()) : the_post();
    global $product;
    $attachment_ids = $product->get_gallery_attachment_ids();
    array_unshift($attachment_ids, get_post_thumbnail_id());
?>
<div class="chosen-product">
    <div class="container wide">
        <div class="row">
            <?php wc_print_notices();?>
            <div class="col-lg-6 image">
                <div class="product-big-photo">
                    <?php
                    // Add a filter to remove srcset attribute from generated <img> tag
                    add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );

                    // Display post thumbnail
                    the_post_thumbnail();

                    // Remove that filter again
                    remove_filter( 'wp_calculate_image_srcset_meta', '__return_null' );
                ?>
                </div>
                <?php if(sizeof($attachment_ids) > 1):?>
                    <div class="slider-wrapper">
                        <div class="owl-carousel owl-theme" id="pictorial-photos-slider">
                            <?php foreach($attachment_ids as $single_photo):?>
                            <div class="item">
                                <?php echo wp_get_attachment_image($single_photo, 'full'); ?>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif;?>
            </div>
            <div class="col-lg-6 desc">
                <h1 class="header">
                    <?php the_title(); ?>
                </h1>
                <?php
                    $price = get_post_meta( get_the_ID(), '_regular_price', true);
                    $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                    $product = new WC_Product_Simple(get_the_ID());
                ?>
                    <div class="price-wrapper simple-product-view">
                        <?php if($sale) : ?>
                        <span class="price">
                            <del class="old-price">
                                <?php echo wc_price($price); ?>
                            </del>
                            <?php echo wc_price($sale); ?>
                        </span>
                        <?php elseif($price) : ?>
                        <span class="price">
                                <?php echo wc_price($price); ?>
                        </span>
                        <?php endif; ?>
                    </div>

                    <?php if($price != 0):?>
                        <div class="button-wrapper">
                            <a class="big-button orange-bg" href="<?php echo get_permalink().'?add-to-cart='.get_the_ID();?>">
                                <i class="fas fa-shopping-basket"></i>
                                dodaj do koszyka
                            </a>
                        </div>
                    <?php endif;?>
                    <p>
                        <?php
                        the_excerpt();
                    ?>
                    </p>
                    <a class="link description-link" href="#">
                        zobacz pełny opis
                    </a>
            </div>
        </div>
    </div>
</div>
<div class="about-product">
    <div class="container wide">
        <div class="border-content">
            <div class="nav nav-tabs" id="about-product" role="tablist">
                <a class="nav-item nav-link active" id="nav-information-tab" href="#nav-information">Opis</a>
<!--                 <a class="nav-item nav-link" id="nav-office-tab" href="#nav-office">Informacje dodatkowe
                </a>
                <a class="nav-item nav-link" id="nav-opinion-tab" href="#nav-opinion">Archiwum Wydań</a> -->
            </div>
            <div class="tab-content page-content" id="about-product-content">
                <div class="tab-pane active" id="nav-information">
                    <p>
                        <?php the_content(); ?>
                    </p>
                </div>
                <div class="tab-pane" id="nav-office">
                    <?php the_field('redakcja'); ?>
                </div>
                <div class="tab-pane" id="nav-opinion">
                    <?php the_field('pole_tekstowe');?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_template_part('template-parts/featured_products');?>

    <?php if ( is_active_sidebar( 'product_bottom' ) ) : ?>
        <div class="commercial-place chosen-product-commercial">
            <div class="container wide">
                <div class="row">
                        <?php dynamic_sidebar( 'product_bottom' ); ?>
                </div>
            </div>
        </div>
    <?php endif;?>

    <?php endwhile; ?>
    <?php get_footer(); ?>