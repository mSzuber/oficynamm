
<div class="col-lg-3 sidebar">
    <div class="mobile-category-filter-menu">
        <div class="scrollable-content">
            <div class="filter-header">
                <h4 class="header">
                    Filtry
                </h4>
                <span class="clear">
                    wyczyść filtry
                </span>
            </div>
            <div class="wrapper">
                <span class="square-header small">
                    Kategorie
                </span>
                <ul class="category-list">
                    <?php 
                        $categories = get_categories(); 
                        foreach($categories as $category):
                    ?>
                    <li>
                        <a href="<?php echo get_term_link($category); ?>">
                            <?php echo $category->name; ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="wrapper">
                <span class="square-header small">
                    Filtry
                </span>
                <form action="<?php echo home_url($wp -> request); ?>" method="GET">
                    <ul class="filter-list custom-inputs">
                        <li>
                            <label class="input-wrapper">
                                <input type="radio" name="filters" value="latest"> Najnowsze
                                <span class="custom-input"></span>
                            </label>
                        </li>
                        <li>
                            <label class="input-wrapper">
                                <input type="radio" name="filters" value="most-read"> Najchętniej czytanie
                                <span class="custom-input"></span>
                            </label>
                        </li>
                        <li>
                            <label class="input-wrapper">
                                <input type="radio" name="filters" value="popular"> Popularne
                                <span class="custom-input"></span>
                            </label>
                        </li>
                    </ul>
                    <button class="price-filter" href="#">
                        filtruj
                    </button>   
                </form>
            </div>
            <div class="close-menu-wrapper">
                <a class="close-button " href="# ">
                    zamknij
                </a>
            </div>
        </div>
    </div>
    <?php if ( is_active_sidebar( 'news_sidebar' ) ) : ?>
        <div class="wrapper">
            <?php dynamic_sidebar( 'news_sidebar' ); ?>
        </div>
    <?php endif; ?>
<!--     <div class="wrapper">
        <span class="square-header small">
            Blok tekstu
        </span>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatem, incidunt? Soluta dignissimos reiciendis explicabo
            perspiciatis adipisci maiores! Officia, nobis. Ullam quaerat placeat consequuntur illum error debitis dolorum
            architecto nulla. Quas!</p>
    </div> -->
</div>