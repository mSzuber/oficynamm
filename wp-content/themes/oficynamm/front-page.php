<?php /* Template Name: Strona główna */ ?>
<?php get_header(); ?>
<div class="main-slider-section">
    <div class="slider-wrapper">
        <div id="home-main-slider" class="owl-carousel owl-theme">
            <?php while( have_rows('slider') ): the_row(); ?>
                <div class="item" style="background-image: url('<?php echo wp_get_attachment_image_url( get_sub_field('tlo'), 'full' );?>')">
                    <div class="container wide">
                        <p><?php the_sub_field('maly_naglowek');?></p>
                        <p><?php the_sub_field('duzy_naglowek');?></p>
                        <a class="button-arrow black" href="<?php the_sub_field('link');?>">
                            czytaj więcej
                            <i class="fas fa-long-arrow-alt-right"></i>
                        </a>
                    </div>
                </div>
            <?php endwhile;?>
        </div>
    </div>
    <div class="container wide">
        <div class="row slider-boxes">
            <?php while( have_rows('zalety') ): the_row();?>
                <div class="col-lg-4 box">
                    <div class="inner">
                        <?php echo wp_get_attachment_image(get_sub_field('ikona') , 'full'); ?>
                        <p><?php the_sub_field('maly_tekst');?>
                            <span><?php the_sub_field('duzy_tekst');?></span>
                        </p>
                    </div>
                </div>
            <?php endwhile;?>
        </div>
    </div>
</div>

<?php get_template_part('template-parts/popular_categories');?>
<?php get_template_part('template-parts/online_platform');?>
<?php get_template_part('template-parts/news');?>

<div class="clients-about-us">
    <div class="container wide">
        <div class="row">
            <?php /*
            <div class="col-md-6 image">
                <?php echo wp_get_attachment_image( get_field('zdjecie_z_lewej') , 'full'); ?>
            </div>
             */ ?>
            <div class="col-lg-6 ml-auto content">
                <span class="title">
                    opinie
                    <span>
                        co mówią o nas klienci
                    </span>
                </span>
                <div class="slider-wrapper">
                    <div id="clients-slider" class="owl-carousel owl-theme">
                        <?php while( have_rows('opinie_klientow') ): the_row();?>
                            <div class="item">
                                <div class="text-wrapper">
                                    <p>
                                        <?php the_sub_field('opinia');?>
                                    </p>
                                </div>
                                <p class="author">
                                    <?php the_sub_field('imie_i_nazwisko');?>, <?php the_sub_field('opis_osoby');?>
                                </p>
                            </div>
                        <?php endwhile;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>