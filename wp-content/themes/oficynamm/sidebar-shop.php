<?php global $wp; ?>
<?php
$current_category = get_queried_object();
?>
<div class="col-lg-3 sidebar">
    <div class="mobile-category-filter-menu">
        <div class="scrollable-content">
            <div class="filter-header">
                <h4 class="header">
                    Filtry
                </h4>
                <span class="clear">
                    wyczyść filtry
                </span>
            </div>
            <div class=" wrapper ">
                <span class="square-header small">
                    Kategorie
                </span>

                <?php
                    $shop_categories = get_terms(array(
                        'taxonomy'      => 'product_cat'
                    ));
                ?>
                <ul class="category-list">
                <?php foreach($shop_categories as $shop_category):?>
                    <li>
                        <a href="<?php echo get_term_link($shop_category);?>">
                            <?php echo $shop_category->name;?>
                        </a>
                    </li>
                <?php endforeach;?>
                </ul>
            </div>
            <div class="wrapper">
                <span class="square-header small">
                    Filtry
                </span>

                <?php
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 1,
                    'orderby'   => 'meta_value_num',
                    'meta_key'  => '_price',
                    'order' => 'desc'
                    );
                $max_price_product_query = new WP_Query($args);
                $max_price_product = wc_get_product( $max_price_product_query->posts[0]->ID );
                ?>

                <div class="price-slider-wrapper">
                    <form action="<?php echo home_url($wp->request); ?>" method="GET">
                        <div id="price-slider" data-max="<?php echo ceil($max_price_product->get_price());?> "></div>
                        <div class="values-wrapper">
                            <input type="hidden" class="value" id="lower-price" name="price-min" value="<?php echo $_GET['price-min'];?>">
                            <input type="hidden" class="value" id="upper-price" name="price-max" value="<?php echo $_GET['price-max'];?>">
                            <span id="lower-user-price"></span>
                            <span id="upper-user-price"></span>
                        </div>
                </div>
                <ul class="filter-list custom-inputs">
                    <?php
                    if( isset($_GET['filters']) ) {
                        $filters = $_GET['filters'];
                    } else {
                        $filters = 'latest';
                    }
                    $filters_names = array(
                        'latest'    => "Najnowsze",
                        'popular'   => "Popularne",
                        'featured'  => "Polecane"
                    );
                    ?>
                    <?php foreach($filters_names as $key => $filter):?>
                        <li style="display: block;">
                            <label class="input-wrapper">
                                <input type="radio" name="filters" value="<?php echo $key;?>" <?php echo ($key==$filters)? 'checked':'';?>> <?php echo $filter;?>
                                <span class="custom-input"></span>
                            </label>
                        </li>
                    <?php endforeach;?>
                </ul>
<!--                 <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton
            " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        - Pakiety -
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="# ">test1</a>
                        <a class="dropdown-item" href="# ">test2</a>
                        <a class="dropdown-item" href="# ">długi test 3</a>
                    </div>
                </div> -->
                <button class="price-filter" href="#">
                    filtruj
                </button>
</form>
            </div>
            <div class="close-menu-wrapper">
                <a class="close-button " href="# ">
                    zamknij
                </a>
            </div>
        </div>
    </div>
    <?php if ( is_active_sidebar( 'shop_sidebar' ) ) : ?>
        <div class="wrapper">
            <?php dynamic_sidebar( 'shop_sidebar' ); ?>
        </div>
    <?php endif; ?>
    <?php if( !empty($current_category->description) && !is_post_type_archive('product')):?>
        <div class="wrapper">
            <span class="square-header small">
                Opis kategorii
            </span>
            <p><?php echo $current_category->description; ?></p>
        </div>
    <?php endif;?>
</div>