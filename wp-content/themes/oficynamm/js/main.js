jQuery(document).ready(function ($) {
  // Home page sliders

  $("#home-main-slider").owlCarousel({
    items: 1,
    nav: true,
    navText: [
      '<i class="fas fa-chevron-left"></i>',
      '<i class="fas fa-chevron-right"></i>'
    ],
    dots: true,
    navContainer: "container-nav",
    autoplay: true
  });

  $("#clients-slider").owlCarousel({
    items: 1,
    nav: true,
    navText: [
      '<i class="fas fa-chevron-left"></i>',
      '<i class="fas fa-chevron-right"></i>'
    ]
  });

  // Single product carousel + showing image from slider

  var chosenProduct = $("#pictorial-photos-slider");
  chosenProduct.owlCarousel({
    items: 3,
    loop: false,
    nav: true,
    mouseDrag: false,
    responsive: {
      992: {
        items: 5
      },
      0: {
        items: 2
      }
    }
  });

  chosenProduct.find(".item").hover(function () {
    var img = $(this).find("img");
    var imgSrc = img.attr("src");
    var mainPhoto = $(".product-big-photo").find("img");
    mainPhoto.attr("width", img.prop("naturalWidth"));
    mainPhoto.attr("height", img.prop("naturalHeight"));
    mainPhoto.attr("src", imgSrc);
  });

  $(".product-duration")
    .find("input")
    .on("click", function () {
      var inputId = $(this).attr("id");
      var offer = $(this)
        .closest(".wrapper")
        .find("[data-offer='" + inputId + "']");
      var anotherOffers = $(this)
        .closest(".wrapper")
        .find(".pricing");

      anotherOffers.removeClass("chosen");
      offer.addClass("chosen");
    });

  // Single product tabs

  $("#about-product a").on("click", function (e) {
    e.preventDefault();

    var wrapper = $(this).closest(".about-product");
    var tabId = $(this).attr("href");
    var tabToShow = wrapper.find(tabId);

    tabToShow.addClass("active");
    $(".tab-pane")
      .not(tabToShow)
      .removeClass("active");

    $(this).addClass("active");
    $("#about-product a")
      .not($(this))
      .removeClass("active");
  });

  // Show/Hide main menu

  $(".navbar-toggler").on("click", function () {
    var menu = $("#main-menu");
    menu.toggleClass("show");
    $(".overlay").toggleClass("show");
  });

  // Sticky class

  var mobileView = window.matchMedia("(max-width: 991px)").matches;

  $(window).scroll(function () {
    var height = $(".menu").offset().top;
    var menu = 1;
    if (!mobileView) {
      menu = 75;
    }
    if (menu < height) {
      $(".menu").addClass("sticky");
    } else {
      $(".menu").removeClass("sticky");
    }
  });

  // Search

  var search = $(".search");

  $(".search-action").on("click", function (e) {
    e.preventDefault();
    search.addClass("show");
  });

  $(".close-search").on("click", function () {
    search.removeClass("show");
  });

  // Filters

  $(".close-menu-wrapper").on("click", function (e) {
    e.preventDefault();
    $("body, html").css("overflow", "auto");
    $(this)
      .closest(".mobile-category-filter-menu")
      .removeClass("show");
  });

  $(".filters").on("click", function (e) {
    e.preventDefault();
    $(".mobile-category-filter-menu").addClass("show");
    $("body, html").css("overflow", "hidden");
  });

  // Filter range

  var priceSlider = document.getElementById("price-slider");

  if (priceSlider) {
    var maxRange = priceSlider.getAttribute("data-max");

    noUiSlider.create(priceSlider, {
      start: [0, parseInt(maxRange)],
      connect: true,
      step: 5,
      range: {
        min: 0,
        max: parseInt(maxRange)
      }
    });

    var minPrice = parseInt(document.getElementById("lower-price").value);
    var maxPrice = parseInt(document.getElementById("upper-price").value);

    if (maxPrice) {
      priceSlider.noUiSlider.set([minPrice, maxPrice]);
    }

    var priceSliderInputValues = [
      document.getElementById("lower-price"),
      document.getElementById("upper-price")
    ];

    var priceSliderUserValues = [
      document.getElementById("lower-user-price"),
      document.getElementById("upper-user-price")
    ];

    priceSlider.noUiSlider.on("update", function (values, handle) {
      priceSliderInputValues[handle].value = values[handle];
      priceSliderUserValues[handle].innerHTML = values[handle] + ' zł';
    });
  }

  // Single product scrolls

  $(".description-link").on("click", function () {
    var button = $(this);
    var buttonData = button.data("show-tab");
    var sectionToScroll = $(".about-product");
    var sectionToScrollHeight = sectionToScroll.offset().top;
    var actualActiveTab = $(".about-product .nav-tabs").find(".active");
    var actualActiveTabId = actualActiveTab.attr("id");

    var actualActiveTabContent = idCurrentSection(actualActiveTabId);

    if (buttonData === actualActiveTabId) {
      scrollToElement(sectionToScrollHeight);
    } else {
      actualActiveTab.removeClass("active");
      $("#" + actualActiveTabContent).removeClass("active");
      $("#nav-information-tab").addClass("active");
      $("#nav-information").addClass("active");
      scrollToElement(sectionToScrollHeight);
    }
  });

  $(".show-variants").on("click", function () {
    var button = $(this);
    var buttonData = button.data("show-tab");
    var sectionToScroll = $(".about-product");
    var sectionToScrollHeight = sectionToScroll.offset().top;
    var actualActiveTab = $(".about-product .nav-tabs").find(".active");
    var actualActiveTabId = actualActiveTab.attr("id");

    var actualActiveTabContent = idCurrentSection(actualActiveTabId);

    if (buttonData === actualActiveTabId) {
      scrollToElement(sectionToScrollHeight);
    } else {
      actualActiveTab.removeClass("active");
      $("#" + actualActiveTabContent).removeClass("active");
      $("#" + actualActiveTabContent).removeClass("active");
      $("#nav-variants-tab").addClass("active");
      $("#nav-variants").addClass("active");

      scrollToElement(sectionToScrollHeight);
    }
  });

  function scrollToElement(section) {
    $("body, html").animate({
        scrollTop: section - 80
      },
      500
    );
    return false;
  }

  function idCurrentSection(element) {
    var actualActiveTabContent = element.split("");
    actualActiveTabContent = actualActiveTabContent.slice(0, -4);
    actualActiveTabContent = actualActiveTabContent.join("");
    return actualActiveTabContent;
  }

  $(".about-product")
    .find(".expand-content")
    .on("click", function () {
      var content = $(this)
        .parent()
        .find(".all-content");
      $(this).toggleClass('show');
      content.toggleClass("show");
    });

  var stars = $('.stars').find('a');

  stars.hover(function () {
    let starNumber = $(this).text().substr(0 - 1);
    for (let i = starNumber; i >= 0; i--) {
      $(stars[i - 1]).addClass('on');
    }
  }, function () {
    $(stars).removeClass('on');
  });

  // Hover in menu
  let screenWidth = screen.width;

  function menuDropdown() {
    if (screenWidth > 991) {
      $('.navbar-nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
      }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
      });
    } else {
      $('.navbar-nav li.dropdown a').addClass('dropdown-toggle');
      $('.navbar-nav li.dropdown .dropdown-menu a').removeClass('dropdown-toggle');
      $('.navbar-nav li.dropdown .dropdown-toggle').attr("href", "#");
      $('.navbar-nav li.dropdown .dropdown-toggle').on("click", function (event) {
        event.preventDefault();
        $(this).parent().find('.dropdown-menu').toggle();
      });
    }
  }

  menuDropdown();

  $(window).resize(function () {
    screenWidth = screen.width;
    menuDropdown();
  });
});