<?php /* Template Name: Podstrona produktu */ ?>
<?php get_header(); ?>
<div class="chosen-product">
    <div class="container wide">
        <div class="row">
            <div class="col-lg-6 image">
                <div class="product-big-photo">
                    <img src="http://via.placeholder.com/455x300" alt>
                </div>
                <div class="slider-wrapper">
                    <div class="owl-carousel owl-theme" id="pictorial-photos-slider">
                        <div class="item">
                            <img src="http://via.placeholder.com/450x300" alt="">
                        </div>
                        <div class="item">
                            <img src="http://via.placeholder.com/400x300" alt>
                        </div>
                        <div class="item">
                            <img src="http://via.placeholder.com/455x100" alt>
                        </div>
                        <div class="item">
                            <img src="http://via.placeholder.com/455x300" alt>
                        </div>
                        <div class="item">
                            <img src="http://via.placeholder.com/455x300" alt>
                        </div>
                        <div class="item">
                            <img src="http://via.placeholder.com/455x300" alt>
                        </div>
                        <div class="item">
                            <img src="http://via.placeholder.com/455x300" alt>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 desc">
                <h1 class="header">
                    Niezbędnik dyrektora szkoły. Zarządzanie oświatą
                </h1>
                <div class="button-wrapper">
                    <a class="big-button orange-bg" href="#">
                        <i class="fas fa-shopping-basket"></i>
                        pokaż warianty
                    </a>
                </div>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Animi voluptatem in deleniti laborum perspiciatis
                    reiciendis repudiandae ex, cumque dolores veniam velit earum iusto, libero non at tempore, ducimus eum
                    nesciunt.
                </p>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint vitae sed ipsum labore! Debitis modi facere
                    tenetur dolor et illo sunt praesentium exercitationem, repellendus deserunt magni veritatis, officiis
                    atque dicta!</p>
                <a class="link" href="#">
                    zobacz pełny opis
                </a>
            </div>
        </div>
    </div>
</div>
<div class="about-product">
    <div class="container wide">
        <div class="border-content">
            <div class="nav nav-tabs" id="about-product" role="tablist">
                <a class="nav-item nav-link active" id="nav-variants-tab" href="#nav-variants">warianty</a>
                <a class="nav-item nav-link" id="nav-information-tab" href="#nav-information">informacje</a>
                <a class="nav-item nav-link" id="nav-office-tab" href="#nav-office">redakcja</a>
                <a class="nav-item nav-link" id="nav-opinion-tab" href="#nav-opinion">opinie</a>
            </div>
            <div class="tab-content" id="about-product-content">
                <div class="tab-pane active" id="nav-variants">
                    <div class="tab-inner">
                        <div class="row">
                            <div class="col-lg-3 left">
                                <div class="inner">
                                    <ul class="option-list">
                                        <li>
                                            lista dostępow w pakiecie
                                        </li>
                                        <li>
                                            dostęp do biezacej platformy asdasd tak tak ta ktakta ask
                                        </li>
                                        <li>
                                            lista dostępow w pakiecie
                                        </li>
                                        <li>
                                            dostęp do biezacej platformy asdasd tak tak ta ktakta ask
                                        </li>
                                        <li>
                                            dostęp do biezacej platformy asdasd tak tak ta ktakta ask
                                        </li>
                                        <li>
                                            lista dostępow w pakiecie
                                        </li>
                                        <li>
                                            dostęp do biezacej platformy asdasd tak tak ta ktakta ask
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-9 no-padding wrapper">
                                <div class="product-duration active">
                                    <span class="text">Wybierz okres</span>
                                    <label class="input-wrapper active" for="six-months">
                                        <input id="six-months" name="months" type="radio" value="6" checked> 6 miesięcy

                                        <span class="custom-input"></span>
                                    </label>
                                    <label class="input-wrapper" for="twelve-months">
                                        <input id="twelve-months" name="months" type="radio" value="12"> 12 miesięcy
                                        <span class="custom-input"></span>
                                    </label>
                                    <label class="input-wrapper" for="twenty-four-months">
                                        <input id="twenty-four-months" name="months" type="radio" value="24"> 24 miesięcy
                                        <span class="custom-input"></span>
                                    </label>
                                </div>
                                <div class="row pricing chosen" data-offer="six-months">


                                    <div class="col-md-4 box">
                                        <div class="inner">
                                            <div class="offer-name">
                                                <span>
                                                    standard
                                                    <br> 6
                                                </span>
                                            </div>
                                            <ul class="about-product-list">
                                                <li>
                                                    <span class="mobile">
                                                        dostęp do biezacej platformy jakieś na pewno
                                                    </span>
                                                    <span class="content">
                                                        3
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        dostęp do biezacej platformy jakieś na pewno
                                                    </span>
                                                    <span class="content">
                                                        80 zł
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Dostęp do bieżącej aktualizacji platformy „RODO KROK PO KROKU”
                                                    </span>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    3/miesięc
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    Bez limitu
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    100%
                                                </li>
                                            </ul>
                                            <div class="summary-price">
                                                <span class="price">
                                                    19634.45 zł
                                                </span>
                                                <a class="button orange-bg" href="#">
                                                    wybierz
                                                </a>
                                            </div>
                                            <div class="box-shadow">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-4 box">
                                        <div class="inner">
                                            <div class="offer-name">
                                                <span>
                                                    vip
                                                    <br> 6
                                                </span>
                                            </div>
                                            <ul class="about-product-list">
                                                <li>
                                                    <span class="mobile">
                                                        dostęp do biezacej platformy jakieś na pewno
                                                    </span>
                                                    <span class="content">
                                                        3
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        dostęp do biezacej platformy jakieś na pewno
                                                    </span>
                                                    <span class="content">
                                                        80 zł
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Dostęp do bieżącej aktualizacji platformy „RODO KROK PO KROKU”
                                                    </span>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    3/miesięc
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    Bez limitu
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    100%
                                                </li>
                                            </ul>
                                            <div class="summary-price">
                                                <span class="price">
                                                    19634.45 zł
                                                </span>
                                                <a class="button orange-bg" href="#">
                                                    wybierz
                                                </a>
                                            </div>
                                            <div class="box-shadow">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-4 box">
                                        <div class="inner">
                                            <div class="offer-name">
                                                <span>
                                                    premium
                                                    <br> 6
                                                </span>
                                            </div>
                                            <ul class="about-product-list">
                                                <li>
                                                    <span class="mobile">
                                                        dostęp do biezacej platformy jakieś na pewno
                                                    </span>
                                                    <span class="content">
                                                        3
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        dostęp do biezacej platformy jakieś na pewno
                                                    </span>
                                                    <span class="content">
                                                        80 zł
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Dostęp do bieżącej aktualizacji platformy „RODO KROK PO KROKU”
                                                    </span>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    3/miesięc
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    Bez limitu
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    100%
                                                </li>
                                            </ul>
                                            <div class="summary-price">
                                                <span class="price">
                                                    19634.45 zł
                                                </span>
                                                <a class="button orange-bg" href="#">
                                                    wybierz
                                                </a>
                                            </div>
                                            <div class="box-shadow">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row pricing" data-offer="twelve-months">
                                    <div class="col-md-4 box">
                                        <div class="inner">
                                            <div class="offer-name">
                                                <span>
                                                    standard
                                                    <br> 12
                                                </span>
                                            </div>
                                            <ul class="about-product-list">
                                                <li>
                                                    <span class="mobile">
                                                        dostęp do biezacej platformy jakieś na pewno
                                                    </span>
                                                    <span class="content">
                                                        3
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        dostęp do biezacej platformy jakieś na pewno
                                                    </span>
                                                    <span class="content">
                                                        80 zł
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Dostęp do bieżącej aktualizacji platformy „RODO KROK PO KROKU”
                                                    </span>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    3/miesięc
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    Bez limitu
                                                </li>
                                                <li>
                                                    <span class="mobile">
                                                        Porady prawnika, psychologa i pedagoga + telefoniczne serwis porad prawnika
                                                    </span>
                                                    100%
                                                </li>
                                            </ul>
                                            <div class="summary-price">
                                                <span class="price">
                                                    19634.45 zł
                                                </span>
                                                <a class="button orange-bg" href="#">
                                                    wybierz
                                                </a>
                                            </div>
                                            <div class="box-shadow">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-4 box">
                                        <div class="inner">
                                            <div class="offer-name">
                                                <span>
                                                    standard
                                                    <br> 12
                                                </span>
                                            </div>
                                            <ul class="about-product-list">
                                                <li>3</li>
                                                <li>80 zł</li>
                                                <li>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>3/miesięc</li>
                                                <li>Bez limitu</li>
                                                <li>100%</li>
                                            </ul>
                                            <div class="summary-price">
                                                <span class="price">
                                                    19634.45 zł
                                                </span>
                                                <a class="button orange-bg" href="#">
                                                    wybierz
                                                </a>
                                            </div>
                                            <div class="box-shadow">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-4 box">
                                        <div class="inner">
                                            <div class="offer-name">
                                                <span>
                                                    standard
                                                    <br> 12
                                                </span>
                                            </div>
                                            <ul class="about-product-list">
                                                <li>3</li>
                                                <li>80 zł</li>
                                                <li>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>
                                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/check-green.png">
                                                </li>
                                                <li>3/miesięc</li>
                                                <li>Bez limitu</li>
                                                <li>100%</li>
                                            </ul>
                                            <div class="summary-price">
                                                <span class="price">
                                                    19634.45 zł
                                                </span>
                                                <a class="button orange-bg" href="#">
                                                    wybierz
                                                </a>
                                            </div>
                                            <div class="box-shadow">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="nav-information">2</div>
                <div class="tab-pane" id="nav-office">3</div>
                <div class="tab-pane" id="nav-opinion">4</div>
            </div>
        </div>
    </div>
</div>
<div class="popular-products chosen-product-popular-products">
    <div class="container wide">
        <div class="col-12 small-big-header-wrapper black">
            <span class="header small">
                sprawdź
            </span>
            <span class="header big">
                polecane produkty
            </span>
        </div>
        <div class="row products-section">
            <div class="col-md-5 wide-section no-padding">
                <div class="col-12 wide-product shop-product">
                    <div class="inner">
                        <div class="product-label-wrapper">
                            <span class="product-label red">
                                polecamy
                            </span>
                            <span class="product-label orange">
                                polecamy
                            </span>
                            <span class="product-label pink">
                                super cena
                            </span>
                        </div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/big-photo-01.png')" alt>
                        <p class="title">
                            Niezbędnik dyrektora szkoły. Zarządzanie oświatą
                        </p>
                        <div class="price-wrapper promotion">
                            <span class="price">
                                2599 zł
                            </span>
                            <span class="promotion-price">
                                2499 zł
                            </span>
                        </div>
                        <a class="shop-button orange" href="#">
                            <i class="fas fa-shopping-basket"></i>
                            do koszyka
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-7 narrow-section no-padding">
                <div class="row">
                    <div class="col-md-4 narrow-product shop-product">
                        <div class="inner">
                            <div class="product-label-wrapper">
                                <span class="product-label red">
                                    polecamy
                                </span>
                            </div>
                            <div class="img-wrapper">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-01.png')" alt>
                            </div>
                            <p class="title">
                                rodo w szkole. krok po kroku
                            </p>
                            <span class="price">
                                195,57 zł
                            </span>
                            <a class="shop-button orange" href="#">
                                <i class="fas fa-shopping-basket"></i>
                                do koszyka
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 narrow-product shop-product">
                        <div class="inner">
                            <div class="product-label-wrapper">
                                <span class="product-label red">
                                    polecamy
                                </span>
                            </div>
                            <div class="img-wrapper">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-02.png')" alt>
                            </div>
                            <p class="title">
                                Oświatowy serwis kadrowy
                            </p>
                            <span class="price">
                                195,57 zł
                            </span>
                            <a class="shop-button orange" href="#">
                                <i class="fas fa-shopping-basket"></i>
                                do koszyka
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 narrow-product shop-product">
                        <div class="inner">
                            <div class="product-label-wrapper">
                                <span class="product-label red">
                                    polecamy
                                </span>
                            </div>
                            <div class="img-wrapper">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-03.png')" alt>
                            </div>
                            <p class="title">
                                rodo w szkole. krok po kroku
                            </p>
                            <span class="price">
                                195,57 zł
                            </span>
                            <a class="shop-button orange" href="#">
                                <i class="fas fa-shopping-basket"></i>
                                do koszyka
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 narrow-product shop-product">
                        <div class="inner">
                            <div class="product-label-wrapper">
                                <span class="product-label red">
                                    polecamy
                                </span>
                            </div>
                            <div class="img-wrapper">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-01.png')" alt>
                            </div>
                            <p class="title">
                                rodo w szkole. krok po kroku
                            </p>
                            <span class="price">
                                195,57 zł
                            </span>
                            <a class="shop-button orange" href="#">
                                <i class="fas fa-shopping-basket"></i>
                                do koszyka
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 narrow-product shop-product">
                        <div class="inner">
                            <div class="product-label-wrapper">
                                <span class="product-label red">
                                    polecamy
                                </span>
                            </div>
                            <div class="img-wrapper">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-02.png')" alt>
                            </div>
                            <p class="title">
                                Oświatowy serwis kadrowy
                            </p>
                            <span class="price">
                                195,57 zł
                            </span>
                            <a class="shop-button orange" href="#">
                                <i class="fas fa-shopping-basket"></i>
                                do koszyka
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 narrow-product shop-product">
                        <div class="inner">
                            <div class="product-label-wrapper">
                                <span class="product-label red">
                                    polecamy
                                </span>
                            </div>
                            <div class="img-wrapper">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sample/small-product-03.png')" alt>
                            </div>
                            <p class="title">
                                rodo w szkole. krok po kroku
                            </p>
                            <span class="price">
                                195,57 zł
                            </span>
                            <a class="shop-button orange" href="#">
                                <i class="fas fa-shopping-basket"></i>
                                do koszyka
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="commercial-place chosen-product-commercial">
    <div class="container wide">
        <div class="row">
            <div class="col-md-6">
                <img src="http://via.placeholder.com/570x315" alt>
            </div>
            <div class="col-md-6">
                <img src="http://via.placeholder.com/570x315" alt>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>