<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        <?php wp_title( '|', true, '' ); ?>
    </title>
    <?php 
        wp_head(); 
        global $woocommerce;
        $count_cart = $woocommerce->cart->cart_contents_count;
    ?>
</head>

<style>
    html {
        margin-top: 0 !important;
    }
</style>

<body>
    <div class="fixed-link">
        <a href="http://oswiata.oficynamm.pl" target="_blank" class="inner">
            <div class="orange-circle">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/fixed-link-book.png" alt="">
            </div>
            <p>mój niezbędnik</p>
        </a>
    </div>
    <div class="overlay"></div>
    <div class="search">
        <span class="close-search-wrapper">
            <i class="fas fa-times close-search"></i>
        </span>
        <form class="search-wrapper" action="<?php echo home_url();?>">
            <input type="search" placeholder="Wpisz szukaną frazę" name="s">
            <button>szukaj</button>
        </form>
    </div>
    <div class="menu">
        <div class="top-bar red-bg">
            <div class="container wide">
                <p>Skontaktuj się z nami:
                    <a href="tel:<?php the_field('numer_telefonu','option');?>">
                        <?php the_field('numer_telefonu','option');?>
                    </a>
                </p>
            </div>
        </div>
        <nav class="navbar navbar-expand-xl">
            <div class="container wide">
                <a class="navbar-brand" href="<?php echo home_url();?>">
                    <img src="<?php echo get_stylesheet_directory_uri();?>/img/logo.png" alt="">
                </a>
                <?php wp_nav_menu( array(
                   'theme_location'    => 'header',
                   'depth'             => 2,
                   'menu_class'        => 'navbar-nav',
                   'container_class'   => 'menu-content',
                   'container_id'      => 'main-menu',
                   'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                   'walker'            => new WP_Bootstrap_Navwalker(),
                ) );
               ?>
                <div class="social-section">
                    <a class="search-action" href="#">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/menu-icons/search.png" alt="">
                    </a>
                    <a href="http://oswiata.oficynamm.pl">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/menu-icons/book.png" alt="">
                    </a>
                    
                    <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) );?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/menu-icons/user.png" alt="">
                    </a>                    

                    <a class="shop" href="<?php echo get_permalink( wc_get_page_id( 'cart' ) ); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/menu-icons/shopping-bag.png" alt="">
                        <?php
                            if(!empty($count_cart)) :
                        ?>
                        <span class="number-of-items">
                            <span>
                                <?php echo $count_cart; ?>
                            </span> 
                        </span>
                        <?php endif; ?>
                    </a>

                </div>
                <button class="navbar-toggler" type="button" aria-label="Toggle navigation">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/menu-icons/menu.svg" alt="">
                </button>
                <div class="close-mobile-menu-wrapper">
                    <img class="close-menu navbar-toggler" src="<?php echo get_stylesheet_directory_uri(); ?>/img/menu-icons/cross-out.svg" alt="">
                </div>
            </div>
        </nav>
    </div>