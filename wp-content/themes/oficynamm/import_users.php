<?php /* Template Name: Import */

global $wpdb; 

echo 'START IMPORT </br>';

$presta = new wpdb('root','plokij12','presta','mysql');

$sql = "SELECT 
	ps_customer.firstname, 
	ps_customer.lastname, 
	ps_customer.email, 
	ps_customer.passwd,
	ps_address.company,
	ps_address.firstname AS address_firstname, 
	ps_address.lastname AS address_lastname,
	ps_address.address1,
	ps_address.address2,
	ps_address.postcode,
	ps_address.city,
	ps_address.phone_mobile,
	ps_address.vat_number
	FROM ps_customer 
	LEFT JOIN ps_address ON ps_address.id_customer = ps_customer.id_customer
	";

$users = $presta->get_results($sql);

foreach($users as $user) {
	$userdata = array(
		'user_login' 	=> $user->email,
		'first_name'	=> $user->firstname,
		'last_name'	 	=> $user->lastname,
		'user_email'	=> $user->email,
		'role'			=> 'customer'
	);

	$user_id = wp_insert_user( $userdata );
	if( is_int($user_id) ) {
		echo $user->email.' - '.$user_id.' - Dodano<br>';
	} else {
		echo $user->email.' - Istnieje<br>';		
	}

	if($user_id) {
		update_user_meta( $user_id, "presta_passwd", $user->passwd);
		update_user_meta( $user_id, "billing_first_name", $user->address_firstname );
		update_user_meta( $user_id, "billing_last_name", $user->address_lastname );
		update_user_meta( $user_id, "billing_company", $user->company );
		update_user_meta( $user_id, "billing_address_1", $user->address1 );
		update_user_meta( $user_id, "billing_address_2", $user->address2 );
		update_user_meta( $user_id, "billing_postcode", $user->postcode );
		update_user_meta( $user_id, "billing_city", $user->city );
		update_user_meta( $user_id, "billing_phone", $user->phone_mobile );
		update_user_meta( $user_id, "billing_email", $user->email );

	}

}