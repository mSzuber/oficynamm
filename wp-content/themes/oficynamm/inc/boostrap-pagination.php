<?php
function fellowtuts_wpbs_pagination($pages = '', $range = 2) 
{  
	$showitems = ($range * 2) + 1;  
	global $paged;
	if(empty($paged)) $paged = 1;
	if($pages == '')
	{
		global $wp_query; 
		$pages = $wp_query->max_num_pages;
	
		if(!$pages)
			$pages = 1;		 
	}   
	
	if(1 != $pages)
	{
	    echo '<nav aria-label="Page navigation" role="navigation">';
        echo '<span class="sr-only">Page navigation</span>';
        echo '<ul class="pagination">';

	 	if($paged > 2 && $paged > $range+1 && $showitems < $pages) 
			echo '<li class="page-item prev"><a class="page-link" href="'.get_pagenum_link(1).'" aria-label="First Page"><i class="fas fa-angle-double-left"></i></a></li>';
	
	 	if($paged > 1 && $showitems < $pages) 
			echo '<li class="page-item prev"><a class="page-link" href="'.get_pagenum_link($paged - 1).'" aria-label="Previous Page"><i class="fas fa-angle-left"></i></a></li>';
    
        echo '<div class="pagination-numbers">';
		for ($i=1; $i <= $pages; $i++)
		{
		    if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
				echo ($paged == $i)? '<li class="page-item active"><span class="page-link">'.$i.'</span></li>' : '<li class="page-item"><a class="page-link" href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
        }
        echo '</div>';
		
		if ($paged < $pages && $showitems < $pages) 
			echo '<li class="page-item next"><a class="page-link" href="'.get_pagenum_link($paged + 1).'" aria-label="Next Page"><i class="fas fa-angle-right"></i></a></li>';  
			
		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) 
			echo '<li class="page-item next"><a class="page-link" href="'.get_pagenum_link($pages).'" aria-label="Last Page"><i class="fas fa-angle-double-right"></i></a></li>';
	 	echo '</ul>';
        echo '</nav>';
        //echo '<div class="pagination-info mb-5 text-center">[ <span class="text-muted">Page</span> '.$paged.' <span class="text-muted">of</span> '.$pages.' ]</div>';	 	
	}
}