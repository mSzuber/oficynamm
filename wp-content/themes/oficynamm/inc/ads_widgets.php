<?php

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wps_ad_w190' );
	register_widget( 'wps_ad_w1140' );
	register_widget( 'wps_ad_w570' );

}
add_action( 'widgets_init', 'wpb_load_widget' );
// Creating the widget 
class wps_ad_w570 extends WP_Widget {

	function __construct() {
		parent::__construct(

			// Base ID of your widget
			'wps_ad_w570', 

			// Widget name will appear in UI
			'Reklama 570 x 315', 

			// Widget description
			array( 'description' => 'Reklama o szerokości 570 pikseli i nieograniczonej wysokości' ) 
		);
	}

// Creating widget front-end

	public function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];
		?>
		<div class="col-md-6">
			<a href="<?php the_field( 'url','widget_'.$widget_id );?>">
			 <?php echo wp_get_attachment_image( get_field( 'reklama','widget_'.$widget_id ), 'full' ); ?>
			</a>
		</div>
		<?php 
	}

// Widget Backend 
	public function form( $instance ) {

	}

// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here
// Creating the widget 
class wps_ad_w1140 extends WP_Widget {

	function __construct() {
		parent::__construct(

			// Base ID of your widget
			'wps_ad_w1140', 

			// Widget name will appear in UI
			'Reklama 1140 x 180', 

			// Widget description
			array( 'description' => 'Reklama o szerokości 1140 pikseli i nieograniczonej wysokości' ) 
		);
	}

// Creating widget front-end

	public function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];
		?>

		<a href="<?php the_field( 'url','widget_'.$widget_id );?>">
		 <?php echo wp_get_attachment_image( get_field( 'reklama','widget_'.$widget_id ), 'full' ); ?>
		</a>

		<?php 
	}

// Widget Backend 
	public function form( $instance ) {

	}

// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here

// Creating the widget 
class wps_ad_w190 extends WP_Widget {

	function __construct() {
		parent::__construct(

			// Base ID of your widget
			'wps_ad_w190', 

			// Widget name will appear in UI
			'Reklama 190 x 600', 

			// Widget description
			array( 'description' => 'Reklama o szerokości 190 pikseli i nieograniczonej wysokości' ) 
		);
	}

// Creating widget front-end

	public function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];
		?>

		<a href="<?php the_field( 'url','widget_'.$widget_id );?>">
		 <?php echo wp_get_attachment_image( get_field( 'reklama','widget_'.$widget_id ), 'full' ); ?>
		</a>

		<?php 
	}

// Widget Backend 
	public function form( $instance ) {

	}

// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here

