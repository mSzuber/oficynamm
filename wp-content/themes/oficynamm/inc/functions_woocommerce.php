<?php
function woocommerce_support() {
   add_theme_support( 'woocommerce' );

}
add_action( 'after_setup_theme', 'woocommerce_support' );



/**
* Change number of products that are displayed per page (shop page)
*/
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
 // $cols contains the current number of products per page based on the value stored on Options -> Reading
 // Return the number of products you wanna show per page.
 $cols = 16;
 return $cols;
}

/**
 * Adds a new column to the "My Orders" table in the account.
 *
 * @param string[] $columns the columns in the orders table
 * @return string[] updated columns
 */


function sv_wc_add_my_account_orders_column( $columns ) {
    $new_columns = array(
        'order-number'       =>  'order-number',
        'order-total'       =>  'order-total',
        'order-status'      =>  'order-status',
        'order-actions'     =>  'order-status'
    );
    return $new_columns;
}
add_filter( 'woocommerce_my_account_my_orders_columns', 'sv_wc_add_my_account_orders_column' );

