<?php get_header(); ?>
<?php if ( function_exists('yoast_breadcrumb') && ! is_front_page()) : ?>
   <div class="breadcrumbs">
       <div class="container">
            <?php yoast_breadcrumb('<p id="breadcrumbs">','</p>');?>
       </div>
   </div>
<?php endif; ?>
<div class="container single-post page-content">
    <div class="row">
        <div class="inner col-lg-12">
            <h1 class="square-header">
                <?php echo get_the_title(); ?>
            </h1>
            <?php the_content(); ?>
        </div>
        <?php //get_sidebar( 'single' ); ?>
    </div>
</div>

<?php get_footer(); ?>